//definir variable 
//var {ya casi no se usa, no tan viable}  
//const { mas usada actualmente}

const express= require('express');
const fs =  require ('fs'); //variale para el fs = npm i fs --save
const app = express();
app.use(express.json()); //para el post
app.use(express.urlencoded({extended:true}));
const port = 3000;


const manejaGet =(req,res) => { //metodo el req lee los datps y res es el que da respuesta de salida
    console.log("Estoy en la pagina home");
    res.json({saludo:'Hola mundo desde Node!'});
}
app.get('/home',manejaGet);
const manejaGet2 =(req,res) => { //metodo el req lee los datps y res es el que da respuesta de salida
    console.log("Estoy en la pagina David");
    res.json({nombre:'David Rasgado De La Cruz'});
}
app.get('/david',manejaGet2);

const  readFile = (err,data)=>{
    if(err) console.log('Hubo un error');
    let info = JSON.parse(data);
    console.log(data);
    return data;
}



app.get('/people', (req, res) =>{
   let data = fs.readFileSync('database/table.json');
   let info = JSON.parse(data) ;
   console.log('Leimos el archivo');
   console.log(info);
    res.json({response: info});
})

app.get('/people/:id',(req,res)=>{ //el /:id es para indicar que el parametro recivira y sera en el url people/1
    //guardamos el id con que se realizo la clase
    let identifier = req.params.id;
    //leemos la informacion de la base de datos
    let data =fs.readFileSync('database/table.json');
    let obJSON = JSON.parse(data);
    //tratamos de buscar el identificador de la base de datos
    let response = null;
    for(let i=0;i<obJSON.length;i++){//para encontrar u  identificador 
        if(obJSON[i].id == identifier){
            response = obJSON[i];
            break;
        }
    }
    if(response == null ){ //el .keys es para saber si hay algo se refiere a los {} de archivo de database
        res.status.apply(404).send();
        return;
    }
    res.json(response);
})

app.post('/people', (req,res)=>{
    console.log(req.body);
    let person =req.body;
    let data = fs.readFileSync('database/table.json');
    let info = JSON.parse(data);
    //agregar validacion 
    let obJSON = JSON.parse(data);
//    let response = null;
    for(let i=0;i<obJSON.length;i++){//para encontrar u  identificador 
        if(obJSON[i].id == person.id){
           res.status(400).json({mensaje: 'Error duplicidad el ID ya existe'});  
           return;
            //break;
        }
    }
    info.push(person);
    fs.writeFileSync('database/table.json', JSON.stringify(info));
    res.status(201).json(person);
    
})

app.listen(port,()=>{
    console.log("El servidor esta escuchando en la url http//localhost:",port);
})